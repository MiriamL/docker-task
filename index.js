const express = require("express")
const app = express()
const path = require("path")

// Share the public folder
app.use(express.static(path.join(__dirname, "public")))

app.get("/", function(req, res) {
    return res.sendFile(
      path.join(__dirname, "public", "index.html"))
})

//Desctructuring from an object
const { PORT = 8080 } = process.env

app.listen(PORT, function() {
  console.log("Server has started on port " + PORT)
})