# Lesson Task: 2.2 Docker

## Requirements 
Docker

## Part 1:
Gcc image pulled and installed with cowsay in it, screenshot:

![Screenshot_part1](pic.png)

## Part 2: 
Created a custom docker image, using these command: <br/>
```bash
docker build https://gitlab.com/MiriamL/docker-task.git#main -t dockertask

docker run -p 5000:8080 docker.io/library/dockertask
```
Shows this on localhost/5000: <br/>

![Screenshot_part2](pic2.png)

## Part 3: 
(Name and describe one use of docker not discussed in class)<br/>
I searched https://hub.docker.com and found Ghost, which is a blogging platform (content creation platform) written in JavaScript and Node.js. It is available as open-source software or as a Docker image which bundles up all the dependencies. Then the user can set up and customize their own self-hosted Ghost blog. Docker allows one to deploy applications like Ghost, without installling packages etc. but still gives the user control of the content. I used ```docker pull ghost``` and went to localhost:3001, and it was all good to go. 

## Maintainer
Miriam Lagergren | [@MiriamL](https://gitlab.com/MiriamL)